# BYN exchange module

Module for get currency exchange rates. Provider national bank of Belarus.

### Install
```
$ npm i @nestlab/byn-exchange
```

### Configuration

```typescript
@Module({
    imports: [
        BynExchangeModule.forRoot({
            useCache: true,
            cacheTtl: 24 * 3600 * 1000, // 1 day
        })
    ],
})
export class AppModule {
}
```

### Usage

```typescript

export class YourService {
    constructor(private readonly converter: BynExchangeConverter) {
    }
    
    async convert1000UsdToByn(): Promise<number> {
        this.converter.convert(Currency.USD).toByn(1000);
    }

    async convert1000BynToUsd(): Promise<number> {
        this.converter.convert(Currency.USD).fromByn(1000);
    }
}

```

Enjoy!
