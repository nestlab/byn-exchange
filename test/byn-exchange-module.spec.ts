import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { BynExchangeConverter, BynExchangeModule, BynExchangeProvider } from '../src';

describe('BYN exchange module', () => {
    let app: INestApplication;

    beforeAll(async () => {
        const testingModule = await Test.createTestingModule({
            imports: [
                BynExchangeModule.forRoot({
                    useCache: true,
                    cacheTtl: 3600 * 1000,
                })
            ],
        }).compile();

        app = testingModule.createNestApplication();
    });

    test('Test BynExchangeProvider provider', () => {
        const guard = app.get(BynExchangeProvider);

        expect(guard).toBeInstanceOf(BynExchangeProvider);
    });

    test('Test BynExchangeConverter provider', () => {
        const guard = app.get(BynExchangeConverter);

        expect(guard).toBeInstanceOf(BynExchangeConverter);
    });
});
