export interface ExchangeRate {
    id: number;
    date: Date;
    symbol: string;
    scale: number;
    name: string;
    officialRate: number;
}
