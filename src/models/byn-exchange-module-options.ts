export interface BynExchangeModuleOptions {
    useCache: boolean;
    cacheTtl?: number;
}
