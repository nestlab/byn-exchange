import { ExchangeRate } from '../../models/exchange-rate';

export class CurrencyConverter {
    constructor(private readonly rate: Promise<ExchangeRate>) {
    }

    async toByn(amount: number): Promise<number> {
        const rate = await this.rate;
        return amount * rate.officialRate / rate.scale;
    }

    async fromByn(amount: number): Promise<number> {
        const rate = await this.rate;
        return amount / rate.officialRate * rate.scale;
    }
}
