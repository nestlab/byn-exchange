import { Injectable } from '@nestjs/common';
import { Currency } from '../../enums/currency';
import { BynExchangeProvider } from '../byn-exchange.provider';
import { CurrencyConverter } from './currency.converter';

@Injectable()
export class BynExchangeConverter {
    constructor(private readonly exchangeProvider: BynExchangeProvider) {
    }

    convert(currency: Currency): CurrencyConverter {
        return new CurrencyConverter(this.exchangeProvider.getRate(currency));
    }
}
