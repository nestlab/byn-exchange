import { InternalExchangeRate } from '../models/internal-exchange-rate';
import { ExchangeRate } from '../models/exchange-rate';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ModelTransformer {
    transform(object: InternalExchangeRate): ExchangeRate {
        return {
            id: object.Cur_ID,
            date: new Date(object.Date),
            symbol: object.Cur_Abbreviation,
            name: object.Cur_Name,
            scale: object.Cur_Scale,
            officialRate: object.Cur_OfficialRate,
        };
    }

    transformArray(objects: InternalExchangeRate[]): ExchangeRate[] {
        return objects.map(item => this.transform(item));
    }
}
