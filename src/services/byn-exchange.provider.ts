import { HttpService, Injectable } from '@nestjs/common';
import { ExchangeRate } from '../models/exchange-rate';
import { InternalExchangeRate } from '../models/internal-exchange-rate';
import { ModelTransformer } from './model.transformer';
import { BynExchangeModuleOptions } from '../models/byn-exchange-module-options';
import { Currency } from '../enums/currency';

@Injectable()
export class BynExchangeProvider {
    private readonly DEFAULT_CACHE_TTL = 24 * 3600 * 1000; // 1 day
    private readonly map: {[currency: string]: ExchangeRate} = {};
    private updatedTimestamp: number = 0;

    constructor(private readonly http: HttpService,
                private readonly modelTransformer: ModelTransformer,
                private readonly options: BynExchangeModuleOptions) {
    }

    async getExchangeRates(): Promise<ExchangeRate[]> {
        return this.http.get<InternalExchangeRate[]>('http://www.nbrb.by/api/exrates/rates', {
            params: {periodicity: 0},
        }).toPromise()
            .then(res => res.data.map(rate => this.modelTransformer.transform(rate)));
    }

    async getRate(currency: Currency): Promise<ExchangeRate> {
        if (!this.options.useCache) {
            return this.getExchangeRates().then(item => item.find(x => x.symbol === currency));
        }

        if (this.needUpdateCache) {
            await this.updateCache();
        }

        return this.map[currency];
    }

    private get needUpdateCache(): boolean {
        return this.updatedTimestamp + (this.options.cacheTtl || this.DEFAULT_CACHE_TTL) < new Date().getTime();
    }

    private async updateCache(): Promise<void> {
        if (!this.options.useCache) {
            return;
        }

        const rates = await this.getExchangeRates();

        for (const rate of rates) {
            this.map[rate.symbol] = rate;
        }

        this.updatedTimestamp = new Date().getTime();
    }
}
