import { DynamicModule, HttpModule, HttpService, Module, Provider } from '@nestjs/common';
import { BynExchangeModuleOptions } from './models/byn-exchange-module-options';
import { ModelTransformer } from './services/model.transformer';
import { BynExchangeProvider } from './services/byn-exchange.provider';
import { BynExchangeConverter } from './services/coverter/byn-exchange.converter';

@Module({})
export class BynExchangeModule {
    static forRoot(options: BynExchangeModuleOptions): DynamicModule {
        const publicProviders: Provider[] = [
            BynExchangeConverter,
            {
                provide: BynExchangeProvider,
                useFactory: (http: HttpService,
                             transformer: ModelTransformer) => new BynExchangeProvider(http, transformer, options),
                inject: [
                    HttpService,
                    ModelTransformer,
                ],
            },
        ];

        const privateProviders: Provider[] = [
            ModelTransformer,
        ];

        return {
            module: BynExchangeModule,
            imports: [HttpModule],
            providers: publicProviders.concat(privateProviders),
            exports: publicProviders,
        };
    }
}
